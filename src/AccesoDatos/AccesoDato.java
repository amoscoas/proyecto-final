package AccesoDatos;


import java.sql.*;

public class AccesoDato {

    private Connection conn;
    private Statement stmt;
//    static final String PROYECTO_CONEXION = "CONEXION.txt";

    public AccesoDato() {
        
    }
    
    public AccesoDato(String driver, String strConexion) throws SQLException, Exception {
        Class.forName(driver);
        conn = DriverManager.getConnection(strConexion);
        stmt = conn.createStatement();
    }

    public AccesoDato(String driver, String url, String user, String pswd) throws SQLException, Exception {
        Class.forName(driver);
        conn = DriverManager.getConnection(url, user, pswd);
        stmt = conn.createStatement();
    }
//Consultas INSERT, UPDATE Y DELETE

    public void ejectuarSql(String query) throws SQLException {
        stmt.execute(query);
    }
//Consultas SELECT

    public ResultSet ejecutarQuery(String query) throws SQLException {
        return stmt.executeQuery(query);
    }

//    public static void crearArchivo() throws IOException {
//        File f = new File(PROYECTO_CONEXION);//constructor
//        if (f.createNewFile()) {
//            System.out.println("Archivo creado correctamente");
//        } else {
//            System.out.println("El archivo ya existe");
//        }
//    }
}
