package AccesoDatos;
//
//import static accesodatos.AccesoDato.PROYECTO_CONEXION;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.sql.SQLException;

public class Conector {

    private static AccesoDato conectorBD = null;

    public static AccesoDato getConector() throws SQLException, Exception {
//
        if (conectorBD == null) {
            FileReader reader = new FileReader("CONEXION.txt");
            BufferedReader buffer = new BufferedReader(reader);
            String url = buffer.readLine();
            conectorBD = new AccesoDato("com.microsoft.sqlserver.jdbc.SQLServerDriver",
                    url);
        }
        return conectorBD;
    }
//        if (conectorBD == null) {
//            conectorBD = new AccesoDato("com.microsoft.sqlserver.jdbc.SQLServerDriver",
//                    "jdbc:sqlserver:\\LAPTOP-9TCMT8BPSQLEXPRESS;"
//                    + "DatabaseName=AEROPUERTO;user=java;password=123456");
//
//        }
//        return conectorBD;
//    }

//    public static String LeerDatos() throws IOException {
//        String dato = "";
//        try {
//            FileReader reader = new FileReader(PROYECTO_CONEXION);
//            BufferedReader buffer = new BufferedReader(reader);
//            dato = buffer.readLine();
//            reader.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.toString());
//
//        }
//        return dato;
//    }
}
